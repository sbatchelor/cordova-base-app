// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.ViewLoader class...');

// define class object/constructor for Database class
cloud.ViewLoader = function (viewSrc, containerSelector) {

    // store view source for this loader eg. '/views/inspection-list.html'
    this.viewSrc = viewSrc;
    this.selector = '#content';

    if (typeof containerSelector == 'string') {
        this.selector = containerSelector;
    }

    /**
    * @description loads the view html into the container
    * @param function initViewCallback called after view is loaded
    */
    this.loadView = function (initViewCallback) {

        var _this = this;

        console.log('loading view from: '+cordova.file.applicationDirectory + "www" + this.viewSrc + ' into ' + this.selector);
        window.resolveLocalFileSystemURL(
            cordova.file.applicationDirectory + "www" + this.viewSrc,
            function (viewFile) { // file found
                // read the view file
                viewFile.file(function(file) {
                    var reader = new FileReader();
                    reader.onload = function() {
                        // load in and render view source
                        console.log("view source: ");
                        console.log(reader.result);
                        console.log('source length' + reader.result.length);
                        $(_this.selector).html(reader.result);

                        if (typeof initViewCallback == 'function') {
                            initViewCallback();
                        }
                    }
                    reader.readAsText(file);
                });
            },
            function (error) {
                console.log('error: failed to view file! ('+this.viewSrc+') '+JSON.stringify(error)+'');
            }
        );
    }
}