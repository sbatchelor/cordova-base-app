// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.DataModel class...');

/**
* @description class to take and store the object models from the salesforce org
* provides a method to create new sObjects
*/
cloud.DataModel = function () {

    // an array containing all sObject names that will be used in app and stored locally in smartstore db.
    this.sObjectTypes = [
        'Account',
        'Contact',
        'Attachment',
        // add more sObject types that this app will use here...
    ];

    // set up all the data models for the objects used in the app
    this.models = {};

    /**
    * @description inits soups and data model
    */
    this.init = function () {

    }

    /**
    * @describe grabs sobject description and passes result to callback
    */
    this.describeSObject = function (sObjectType, callback) {
        forceClient.metadata(
            sObjectType,
            function (result) {
                console.log('sObjectType describe: ' + JSON.stringify(result));
                if (typeof callback == 'function') {
                    callback(result);
                }
            },
            function (error) {
                console.log('sObjectType describe error: ' + JSON.stringify(error));
            }
        );
    }

    /**
    * @description grabs object models for each of the listed sObject types from salesforce and updates local datamodels
    */
    this.updateSObjectModels = function () {
        // for individual object types get detailed info
        for (i in this.sObjectTypes) {
            var type = this.sObjectTypes[i];
            // IIEF for describing individual sObjectTypes
            (function (sObjectType) {
                datamodel.describeSObject(
                    sObjectType,
                    function (objectDescription) {
                        var thisModelFields = [];
                        for (j in objectDescription.fields) {
                            thisModelFields.push(objectDescription.fields[j].name);
                        }
                        datamodel.models.push({'name: ': sObjectType, 'fields': thisModelFields});
                        // register object type soups
                        db.registerSObjectSoup(sObjectType, objectDescription.fields);
                    }
                );
            })(type);
        }
    }

    /**
    * @description creates a new object based on the object model for a particular sObject name
    * @param String sObjectType
    */
    this.newSObject = function (sObjectType) {
        return {'Id': null};
    }

    /**
    * @description grabs metadata for org objects
    * returns list of sObjects to callback function
    */
    this.getGlobalSObjectList = function (callback) {
        // first get the global describe which lists object types
        forceClient.describeGlobal(
            function (result) {
                if (typeof callback == 'function') {
                    callback(result.sobjects);
                } else {
                    console.log('updateObjectModels result: '+JSON.stringify(result));
                }
            },
            function(error) {
                console.log('updateObjectModels error: '+JSON.stringify(result));
            }
        );
    }

    this.init();
}