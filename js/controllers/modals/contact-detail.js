// set namespace "cloud.controller.modals"
var cloud = cloud || {};
cloud.controller = cloud.controller || {};
cloud.controller.modal = cloud.controller.modal || {};
console.log('loading cloud.controller.modal.contactDetail class...');

/**
* @description contact detail view controller
*/
cloud.controller.modal.ContactDetail = function () {

    this.init = function() {

        modals.contactDetail = this;
    }


    this.showContactDetails = function(contactId) {
            this.contactId = contactId;



            // add take picture button click handler
            $('.takeContactPic').on('click', function () {
                var userId = $(this).data('userid');
                var userName = $(this).data('username');

                camera.takePicture(function (imageUri) {
                    // add the picture to the beginning picture collection
                    var imageContainer = '<li class="table-view-cell media">';
                    imageContainer += '<img class="media-object pull-left contact-image" src="'+imageUri+'" />';
                    imageContainer += '</li>';
                    $('ul.contactpics').prepend(imageContainer);

                    // get raw image data from file so we can stream it to salesforce
                    camera.getRawData(imageUri, function (imageData) {
                       // attach picture to inspection object in salesforce
                       upload.uploadSObjectAttachment(
                           modals.contactDetail.contactId,
                           Math.round(new Date().getTime()/1000),
                           imageData
                       );
                    });
                });
            });

            this.showContactImages();
    }

    /**
    * @description queries for attached images and displays them on the page
    */
    this.showContactImages = function () {

        console.log('showContactImages:');

        var soql = 'SELECT ' +
            'Id, ' +
            'Body, ' +
            'BodyLength, ' +
            'Description, ' +
            'ContentType, ' +
            'ParentId ' +
            'FROM Attachment ' +
            'WHERE ParentId = \''+this.contactId+'\' '+
            'AND ContentType LIKE \'image%\'';
        console.log('query for pics: '+soql);

        db.query(soql, function(result) {
            console.log('showContactImages: soql query success callback');
            console.log(JSON.stringify(result));
            var images = result.records;
            
            for (i in images) {
                // iief for downloading and displaying images
                (function (imageId, contentType, parentId) {
                    download.downloadFile(images[i].Id, function(data) {
                        var imageContainer = '<li class="table-view-cell media">';
                        imageContainer += '<img class="media-object pull-left contact-image" src="data:'+contentType+';base64,'+data+'" />';
                        imageContainer += '</li>';
                        $('ul.contactpics[data-contactid='+parentId+']').append(imageContainer);
                    });
                })(images[i].Id, images[i].ContentType, images[i].ParentId);
            }

        });
    }


    this.init();
}
