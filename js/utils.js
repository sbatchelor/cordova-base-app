// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.Utils class...');

// define class object/constructor for Utils class
cloud.Utils = function () {
    /**
    * @description returns true if parameter looks like a valid SF id, false otherwise
    */
    this.validId = function (id) {
        if (typeof id == 'string') {
            if (id.match(/^[\w]{5,30}$/i)) {
                return true
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /**
    * @description converts a 15 digit Id (case sensitive) to an 18 digit Id (case insensitive)
    */
    this.convertShortIdToLong = function (shortId) {
        var longId = null;
        if (shortId.length == 15) {
            var s = "";
            for (var i = 0; i < 3; i++) {
                var f = 0;
                for (var j = 0; j < 5; j++) {
                    var c = shortId.charAt(i * 5 + j);
                    if (c >= "A" && c <= "Z") f += 1 << j;
                }
                s += "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".charAt(f);
            }
            longId = shortId + s;
        }
        return longId;
    }

    /**
    * @description faster than native window.btoa method.
    */
    this.base64ArrayBuffer = function (arrayBuffer) {
        var base64    = '';
        var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        var bytes         = new Uint8Array(arrayBuffer);
        var byteLength    = bytes.byteLength;
        var byteRemainder = byteLength % 3;
        var mainLength    = byteLength - byteRemainder;

        var a, b, c, d;
        var chunk;

        // Main loop deals with bytes in chunks of 3
        for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048)   >> 12; // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032)     >>  6; // 4032     = (2^6 - 1) << 6
            d = chunk & 63;               // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
        }

        // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
            chunk = bytes[mainLength];

            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3)   << 4; // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + '==';
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008)  >>  4; // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15)    <<  2; // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
        }

        return base64;
    }
}