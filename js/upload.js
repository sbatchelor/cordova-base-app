// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.Upload class...');

// define class object/constructor for Upload class
cloud.Upload = function () {

    // array of file uploads
    this.uploadQueue = new Array();

    /**
    * @description tests the connection to the remote database.
    */
    this.remoteEnabled = function () {
        if (navigator.network.connection.type == Connection.NONE){
            return false;
        } else {
            return true;
        }
    }

    /**
    * @description attempts to upload picture file as attachment. on success shows a message
    * on failure the picture is then added to a queue of attachments to be uploaded when
    * connection is restored.
    */
    this.uploadSObjectAttachment = function (sObjectId, fileRef, imageData) {
        if (this.remoteEnabled()) {
            console.log('Upload.uploadSObjectAttachment -- uploading file attachment');
            console.log('sObjectId = ' + sObjectId);
            console.log('fileRef = ' + fileRef);
            console.log('imageData = ' + imageData);
            forceClient.create(
                'Attachment',
                {
                    'ParentId': sObjectId,
                    'Name': fileRef,
                    'ContentType': 'image/jpg',
                    'Body': imageData
                },
                function (data) { // success
                    // show upload successful message?
                    console.log('Upload.uploadSObjectAttachment -- success!');
                    console.log('data: '+JSON.stringify(data));
                },
                function (error) { // error
                    // add to file upload queue? or try again? depends on error type...
                    console.log('Upload.uploadSObjectAttachment -- error');
                    console.log('error: '+JSON.stringify(error));

                    // todo: check what type of error this is and either try to re-upload or display a real error message
                    // limit re-uploads to n times
                }
            );
        } else {
            console.log('Upload.uploadSObjectAttachment -- adding file attachment to offline queue');
            this.addFileToUploadQueue(inspectionId, fileRef, imageData);
        }
    }

    /**
    * @description adds a file to the upload queue
    */
    this.addFileToUploadQueue = function (sObjectId, fileRef, imageData) {
        this.uploadQueue.push({
            'sObjectId': sObjectId,
            'fileRef': fileRef,
            'imageData': imageData
        });
    }

    /**
    * @description add listener for network reconnect event. if there are any uploads/attachments in the
    * queue then attempt to upload them
    */
    document.addEventListener(
        "online",
        function () {
            // clone upload queue into new array
            var queue = upload.uploadQueue.slice(0); // shorthand way to clone array in js
            // clear the upload queue (on failure files will be added back into the queue)
            upload.uploadQueue = new Array();
            // process each upload in the queue
            for (i in queue) {
                var item = queue[i];
                console.log('upload.uploadQueue item: '+JSON.stringify(item));
                upload.uploadSObjectAttachment(item.sObjectId, item.fileRef, item.imageData);
            }
        }
    );
};