// set namespace "cloud.controllers"
var cloud = cloud || {};
cloud.controller = cloud.controller || {};
console.log('loading cloud.controller.ContactList class...');

/**
* @description contact list view controller
*/
cloud.controller.ContactList = function () {

    /**
    * @description constructor for the user list view
    */
    this.init = function() {
        controller = this;

        this.contactsList = new Array();

        this.viewSrc = '/views/contact-list.html';
        var vl = new cloud.ViewLoader(this.viewSrc);
        vl.loadView(this.initView);
    }

    /**
    * @description initializes building inspection list view controller
    */
    this.initView = function () {
        /*************************** set title bar ******************************/
        $('#navLeft, #navRight').children('button').remove(); // clear the buttons from the header
        $('#title').text('Contact List');
        /************************* end set title bar ****************************/

        /****************** register interface event handlers *******************/

        /**************** end register interface event handlers *****************/

        // populate list view
        controller.showContactList();
    }

    /**
    * @description queries for list of building inspections and displays results in a single column
    * list view
    */
    this.showContactList = function() {
        // use the new datamodel objects
        var soql = 'SELECT Id, Name ' +
            'FROM Contact ' +
            'LIMIT 100';

        db.query(soql, function(result) {
            console.log('showContactList: soql query success callback');
            console.log('query result: '+JSON.stringify(result));
            var contactsList = new Array();
            if (result.records) {
                contactsList = result.records;
            }
            var contactsListHtml = '';
            for (var i = 0; i < contactsList.length; i++) {
                console.log('contact: ' + JSON.stringify(contactsList[i]));
                contactsListHtml += '<li class="table-view-cell">' +
                    '<div class="media-body contact-link" data-contactid="'+contactsList[i].Id+'">' +
                    contactsList[i].Name +
                    '</div>' +
                    '</li>';
                    
                    // update local data store
                    var toUpsert = contactsList[i];
                    db.updateLocal('Contact', toUpsert.Id, toUpsert);
            }

            $('#contactList').html(contactsListHtml);

            $('.contact-link').on('click', function (ev) {
                // open the contact detail view pass in the contact id
                modals.contactDetail.showContactDetails($(this).data('contactid'));
                $("#contactModal").addClass("active");
            });



        });
    }

    this.init();
}