// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.Database class...');

// constants
var IS_GLOBAL_STORE = false;


// define class object/constructor for Database class
cloud.Database = function () {

    // set auto increment
    if (!storage.getItem('autoIncrement')) {
        storage.setItem('autoIncrement', 0);
    }

    // enable smartsync
    this.smartSync = cordova.require("com.salesforce.plugin.smartsync");

    // init the auto increment for database indexes
    // when no connection is present this will be our temporary id of inserted record
    this.autoIncrement = parseInt(storage.getItem('autoIncrement'));

    /************************** soup registration methods **************************/

    /**
    * @describe registers a new soup (table like struct) for SObject type using metadata from describeSObject
    */
    this.registerSObjectSoup = function(sObjectType, sObjectFields) {

        console.log('Database: registering ' + sObjectType + ' sObject soup');

        // create soup index spec
        var sObjectIndexSpec = new Array();
        for (i in sObjectFields) {
            sObjectIndexSpec.push({'path': sObjectFields[i], 'type': 'string'});
        }

        console.log('Database: field spec for ' + sObjectType + ' object: '+JSON.stringify(sObjectIndexSpec));

        // register sobject soup with smartstore
        navigator.smartstore.registerSoup(
            sObjectType,
            sObjectIndexSpec,
            function (soupName) {
                // success
                console.log('Database: registered ' + soupName + ' soup');
            },
            function () {
                // error
                console.log('Database: ERROR in ' + soupName + ' soup registration');
            }
        );
    }

    /************************ end soup registration methods ************************/

    /**
    * @description tests the connection to the remote database.
    */
    this.remoteEnabled = function () {
        if (navigator.network.connection.type == Connection.NONE){
            return false;
        } else {
            return true;
        }
    }


    /**************************** smartsync operations ***************************/

    // TODO: implement sync functionality using these methods once we can find documentation

    this.syncDown = function (query, objName, callback) {
        // conflicting/incomplete documentation on these operations
        this.smartSync.syncDown(
            IS_GLOBAL_STORE,
            {'type': 'soql', 'query': query},
            objName,
            {mergeMode: Force.MERGE_MODE_DOWNLOAD.LEAVE_IF_CHANGED},
            callback
        );
    }

    this.syncUp = function (objName, fieldList, callback) {
        // conflicting/incomplete documentation on these operations
    }

    /************************** end smartsync operations *************************/

    /*************************** database operations *****************************/
    /**
    * @description public query method. handles testing connection and returns
    * either the local cache result or the remotely queried result when
    * connection is not available.
    */
    this.query = function (queryString, successCallback) {
        if (this.remoteEnabled()) {
            this.queryRemote(queryString, successCallback);
        } else {
            this.queryLocal(queryString, successCallback);
        }
    }

    /**
    * @description private helper method for sending SOQL queries to Salesforce
    * and sends result as parameter to success callback.
    */
    this.queryRemote = function (queryString, successCallback) {
        console.log('running remote query');
        console.log('forceTK Client:');
        console.log(JSON.stringify(forceClient));
        forceClient.query(
            queryString,
            successCallback,
            function(error) {
                console.log('remote SOQL query failed:');
                console.log(queryString);
                console.log('error:');
                console.log(JSON.stringify(error));
            }
        );
    };

    /**
    * @description private helper method for querying local smartstore database
    * converts SOQL query to smart query and returns result set to success
    * callback function
    */
    this.queryLocal = function (queryString, successCallback) {

        // TODO: improve this function or find a way to directly query the underlying SQLLite tables...
        // this is not a great solution and has very limited funcitonality.

        console.log('running local query');
        console.log('original soql query:');
        console.log(queryString);

        // convert soql query into smart query
        // start by deconstructing SOQL query
        // get object type
        var objTypeMatch = queryString.match(/FROM ([\w]+)/i);
        var objType = objTypeMatch[1];

        // get field list
        var fieldListMatch = queryString.match(/SELECT ((([\w_\.]+),? )+)FROM/i);
        var fieldListString = fieldListMatch[1];
        fieldListString = fieldListString.replace(/, /g, ',');
        fieldListString = fieldListString.replace(' ', '');
        var fields = fieldListString.split(',');

        // get where field operator value lists
        var whereClauseMatch = queryString.match(/WHERE (([\w]+) ? ([!|<|>]?=)) \'([\w]+)\'/i);
        if (whereClauseMatch) {
            var whereClauseField = whereClauseMatch[2];
            var whereClauseOperator = whereClauseMatch[3];
            var whereClauseValue = whereClauseMatch[4];
        }

        // start building smart query
        var smartQuery = 'SELECT ';
        // add fields
        for (f in fields) {
            smartQuery += '{' + objType + ':' + fields[f] + '}, ';
        }
        // remove trailing comma
        smartQuery = smartQuery.slice(0, -2) + ' ';
        smartQuery += 'FROM {' + objType + '} ';
        // add where clause
        if (whereClauseMatch) {
            smartQuery += 'WHERE {' + objType + ':' + whereClauseField + '} ';
            smartQuery += whereClauseOperator + ' ';
            smartQuery += '\'' + whereClauseValue + '\'';
        }

        console.log('smartQuery: '+smartQuery);

        var querySpec = navigator.smartstore.buildSmartQuerySpec(smartQuery, 100);

        var callback = successCallback;
        // run the query
        navigator.smartstore.runSmartQuery(IS_GLOBAL_STORE, querySpec, function(result) {
            result.records = new Array();

            for (i in result.currentPageOrderedEntries) {

                var r = {};
                for (f in fields) {
                    if (fields[f].indexOf('.') > 0) {
                        $.extend(r, db.convertFieldsToObjects(fields[f], result.currentPageOrderedEntries[i][f]));
                    }
                    r[fields[f]] = result.currentPageOrderedEntries[i][f];
                }

                result.records.push(r);
            }

            console.log('smart query result.records: '+JSON.stringify(result.records));

            delete result.currentPageOrderedEntries;

            callback(result);
        }, function(error) {
            console.log('Error in smartquery: '+JSON.stringify(error));
        });
    };

    /**
    * @description converts relational smartstore paths such as "Room__c.Building__c.Name" into objects/subobjects
    * accessible as js objects
    */
    this.convertFieldsToObjects = function (path, value) {
        var subObject = {};
        if (path.indexOf('.') != -1) {
            subObject[path.substr(0, path.indexOf('.'))] = this.convertFieldsToObjects(path.substr(path.indexOf('.') + 1), value);
        } else {
            subObject[path] = value;
        }
        return subObject;
    }

    /**
    * @description insert an object, handles object syncing
    */
    this.insert = function(objType, updateFields, callback) {
        var tempId = '_' + (++db.autoIncrement);
        storage.setItem('autoIncrement', db.autoIncrement);
        updateFields.Id = tempId;
        if (this.remoteEnabled()) {
            console.log('local insert to "' + objType + '" object');
            this.insertLocal(objType, updateFields);
            console.log('remote insert to "' + objType + '" object');
            this.insertRemote(objType, updateFields, callback);
        } else {
            console.log('local insert to "' + objType + '" object');
            updateFields['needsSync'] = 'true';
            this.insertLocal(objType, updateFields);
            console.log('remote insert delayed... waiting on connection...');
        }
    };

    this.insertRemote = function (objType, updateFields, callback) {

        var tempId = updateFields.Id;
        delete updateFields.Id;

        console.log('insert remote fields: ' + JSON.stringify(updateFields));
        forceClient.create(
            objType,
            updateFields,
            function (result) { // success
                console.log('remote insert success: '+JSON.stringify(result));

                // update the local record's id
                db.updateLocal(objType, tempId, {'Id': result.id});

                $(document).trigger("RemoteRecordInsert", updateFields);
                if(typeof callback == 'function'){
                    callback(result.id);
                }

            },
            function (error) { // error
                console.log('remote insert error! error: ' + JSON.stringify(error));
            }
        );
    };

    this.insertLocal = function (objType, updateFields) {
        var localUpdateFields = JSON.parse(JSON.stringify(updateFields));
        console.log('updateFields:');
        console.log(JSON.stringify(localUpdateFields));
        navigator.smartstore.upsertSoupEntries(
            IS_GLOBAL_STORE,
            objType, // soup name is same as obj type eg. "Building_Inspection__c"
            [localUpdateFields],
            function () {
                console.log('local update success!');
                $(document).trigger("LocalRecordInsert", updateFields);
            },
            function (error) {
                console.log('local update error! error: '+JSON.stringify(error));
            }
        );
    }

    /**
    * @description update function, updates locally then attempts to update remotely
    *   and adds to the update queue on remote update fail
    */
    this.update = function(objType, objId, updateFields) {
        if (this.remoteEnabled()) {
            console.log('local update to "' + objType + '" object Id=' + objId);
            this.updateLocal(objType, objId, updateFields);
            
            if (objId == '' || null) {
                console.log('remote upsert to "' + objType + '" object Id=' + objId);
                this.upsertRemote(objType, updateFields);
            } else {
                console.log('remote update to "' + objType + '" object Id=' + objId);
                this.updateRemote(objType, objId, updateFields);
            }
        } else {
            console.log('local update to "' + objType + '" object Id=' + objId);
            updateFields['needsSync'] = 'true';
            this.updateLocal(objType, objId, updateFields);
            console.log('remote update delayed... waiting on connection...');
        }
    };

    this.updateRemote = function (objType, objId, updateFields) {
        console.log('update remote fields: '+JSON.stringify(updateFields));
        forceClient.update(
            objType,
            objId,
            updateFields,
            function () { // success
                console.log('remote update success');
                $(document).trigger( "RemoteRecordUpdate", objId, updateFields);
            },
            function (error) { // error
                console.log('remote update error! error: '+JSON.stringify(error));
            }
        );
    };

    this.updateLocal = function (objType, objId, updateFields) {

        console.log('db.updateLocal: ');
        console.log(JSON.stringify(updateFields));

        var localUpdateFields = JSON.parse(JSON.stringify(updateFields));
        localUpdateFields.Id = objId;
        console.log('updateFields:');
        console.log(JSON.stringify(localUpdateFields));
        navigator.smartstore.upsertSoupEntriesWithExternalId(
            IS_GLOBAL_STORE,
            objType, // soup name is same as obj type eg. "Building_Inspection__c"
            [localUpdateFields],
            'Id',
            function () {
                console.log('local update success!');
                $(document).trigger( "LocalRecordUpdate", objId, updateFields);
            },
            function (error) {
                console.log('local update error! error: '+JSON.stringify(error));
            }
        );
    }

    /**
    * @description provide an upsert function for convenience
    */
    this.upsert = function (objType, updateFields) {
        console.log('Upsert called with Id = '+updateFields.Id);

        var objId = updateFields.Id;
        delete updateFields.Id; // remove Id field since salesforce will throw an error if it is provided

        if (objId == '') { // no id, create new record
            this.insert(objType, updateFields);
        } else { // update record
            this.update(objType, objId, updateFields);
        }
    };

    /**
    * @description provide a delete function
    */
    this.del = function (objType, id) {
        console.log('Delete called with objType = ' +objType);
        forceClient.del(objType, id);
    }
    /************************* end database operations ***************************/

    /******************************** event handlers *****************************/
    /**
    * @description handles smartsync events such as syncup and syncdown progress, errors and completions
    */
    this.smartSyncEventHandler = function (event) {
        if (event.type == 'syncUp') {

        } else if (event.type == 'syncDown') {

        } else {
            console.log();
        }
    }

    /**
    * @description handles online state of app. attempts to push updates that have been delayed
    */
    this.onlineEventHandler = function (event) { // online event handler
        // query db for Building_Inspection__c objects that need remote update push
        console.log('online event fired... checking for pending remote updates...');

        /****************************** sObject sync **********************************/
        for (i in datamodel.sObjectTypes) {
            // IIEF for sObject sync operation
            (function (sObjectType) {

                var querySpec = navigator.smartstore.buildSmartQuerySpec(
                    "SELECT {"+sObjectType+":_soup} FROM {"+sObjectType+"} WHERE {"+sObjectType+":needsSync} = 'true'",
                    100
                );

                navigator.smartstore.runSmartQuery(querySpec, function (result) {
                    console.log(sObjectType + ' needing sync results:');
                    console.log(JSON.stringify(result.currentPageOrderedEntries));
                    var toUpdate = result.currentPageOrderedEntries;
                    // perform the remote update
                    for (i in toUpdate) {
                        var id = toUpdate[i][0].Id;

                        var fieldsToUpdate = {};
                        for (k in toUpdate[i][0]) {
                            fieldsToUpdate[k] = toUpdate[i][0][k];
                        }
                        delete fieldsToUpdate['Id']

                        if (utils.validId(id)) {
                            // has a valid id so we can just update the remote record

                            db.updateRemote(
                                sObjectType,
                                id,
                                fieldsToUpdate
                            );

                        } else {
                            db.insertRemote(
                                sObjectType,
                                fieldsToUpdate
                            );
                        }
                        // unset needs sync flag
                        db.updateLocal(
                            sObjectType,
                            id,
                            {
                                'needsSync': false
                            }
                        );
                    }
                });
            })(type);
        }
        /**************************** end sObject sync ********************************/
    }
    /******************************** end event handlers *****************************/

    /************************** event listener registration **************************/
    document.addEventListener(
        "sync",
        this.smartSyncEventHandler
    );

    document.addEventListener(
        "online",
        this.onlineEventHandler,
        false
    );
    /*********************** end event listener registration ************************/

    /********************************* for testing ***********************************/
    this.clearLocalDatabase = function () {
        for (i in datamodel.sObjectTypes) {
            navigator.smartstore.clearSoup(datamodel.sObjectTypes[i]);
        }
    }
    /********************************* for testing ***********************************/
}