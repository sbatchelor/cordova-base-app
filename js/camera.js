// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.Camera class...');

// define class object/constructor for Camera class
cloud.Camera = function () {

    // set the default camera options
    this.options = {
        'quality': 50,
        'destinationType': navigator.camera.FILE_URI,
        'sourceType': navigator.camera.CAMERA,
        'allowEdit': false,
        'encodingType': navigator.camera.JPEG,
        'targetWidth': 800,
        'targetHeight': 600,
        'mediaType': navigator.camera.PICTURE,
        'correctOrientation': true,
        'cameraDirection': navigator.camera.BACK
    };

    /**
    * @description switches context to camera and calls success callback when context is
    * switched back with an image
    * @param function successCallback (string imageUri)
    */
    this.takePicture = function (successCallback) {
        console.log('navigator.camera: ');
        console.log(JSON.stringify(navigator.camera));
        console.log('Camera options: ');
        console.log(JSON.stringify(this.options));
        console.log('navigator.camera.FILE_URI = ' + navigator.camera.FILE_URI);
        // switch to picture taking context and wait for context switch back to this app
        navigator.camera.getPicture(successCallback, this.cameraError, this.options);
    }

    this.cameraError = function (error) {
        console.log('Camera error: ' + JSON.stringify(error));
    }

    /**
    * @description returns raw image data for a fileUri to the supplied callback function
    * @param string fileUri
    * @param function callback(string imageData)
    */
    this.getRawData = function (fileUri, callback) {
        console.log('Camera.getRawData - fileUri: ');
        console.log(fileUri);
        // grab the file reference
        resolveLocalFileSystemURL(fileUri, function (fileEntry) {
            console.log('fileEntry: ');
            console.log(JSON.stringify(fileEntry));
            // turn file reference into File object (which inherits from Blob)
            fileEntry.file(function (file) {
                console.log('fileEntry.file: ');
                console.log(JSON.stringify(file));
                var reader = new FileReader();
                reader.onloadend = function() {
                    // extract raw base64 data from data blob
                    var imageData = reader.result.split(',')[1];
                    callback(imageData);
                }
                reader.readAsDataURL(file);
            }, function (error) {
                console.log('File read error: '+JSON.stringify(error));
            });
        });


    }
}