// set namespace "cloud"
var cloud = cloud || {};
console.log('loading cloud.Download class...');

// define class object/constructor for Download class
cloud.Download = function () {

    // Internal method to generate the key/value pairs of all the required headers for xhr.
    this.getRequestHeaders = function(client) {
        var headers = {};
        
        headers[client.authzHeader] = "Bearer " + client.sessionId;
        headers['Cache-Control'] = 'no-store';
        headers["X-Connect-Bearer-Urls"] = true;
        if (client.userAgentString !== null) {
            headers['X-User-Agent'] = client.userAgentString;
        }
        return headers;
    }
    
    this.init = function () {
        
    }
    
    /**
    * @description downloads an attachment object and passes base64 string to callback
    */
    this.downloadFile = function (attachmentId, callback, error) {
        var req = new XMLHttpRequest();
        
        req.open('GET', forceClient.instanceUrl + '/services/data/v36.0/sobjects/Attachment/'+attachmentId+'/Body', true);
        
        req.responseType = 'arraybuffer';
        
        var headers = this.getRequestHeaders(forceClient);
        
        for (name in headers) {
            req.setRequestHeader(name, headers[name]);
        }
        
        req.onload = function (ev) {
            console.log('ev: '+JSON.stringify(ev));
            console.log('req.response = '+req.response);
            var buffer = req.response;
            if (buffer) {
                console.log('arraybuffer read!');
                var base64Data = utils.base64ArrayBuffer(buffer);
                if (typeof callback == 'function') {
                    callback(base64Data);
                }
            } else {
                console.log('file download failed');
                if (typeof error == 'function') {
                    error();
                }
            }
        }
        
        req.send(null);
    }
    
    this.init();
}