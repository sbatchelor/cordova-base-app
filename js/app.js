console.log('loading app...');
var app = app || {};

var forceClient; // initialized in authenticateUser()
var oauthPlugin; // salesforce oauth plugin

var db; // local and remote data transaction handler
var datamodel; // local object schema definitions
var camera; // camera feature handle
var upload; // upload handler for attachments/images sent to salesforce
var download; // download handler for attachments/images sent to salesforce
var utils; // utility functions and misc.

var controller = {}; // stores current view controller

var modals = {}; // stores modal controllers

// warning! localStorage only stores strings!
var storage = window.localStorage;

if (storage.getItem('isAuthenticated') != 'true') {
    storage.setItem('isAuthenticated', 'false');
}

(function () {
    "use strict";

    /***************** adding platform (ios/android) specific css ******************/
    var platformStyle = document.createElement('link');
    platformStyle.setAttribute('rel', 'stylesheet');
    var customPlatformStyle = document.createElement('link');
    customPlatformStyle.setAttribute('rel', 'stylesheet');
    if (/Android/.test(navigator.userAgent)) {
        platformStyle.setAttribute('href', 'css/ratchet-theme-android.css');
        customPlatformStyle.setAttribute('href', 'css/custom-android.css');
    } else if (/iPhone/.test(navigator.userAgent)) {
        platformStyle.setAttribute('href', 'css/ratchet-theme-ios.css');
        customPlatformStyle.setAttribute('href', 'css/custom-ios.css');
    }
    document.querySelector('head').appendChild(platformStyle);
    document.querySelector('head').appendChild(customPlatformStyle);
    /*************** end adding platform (ios/android) specific css ****************/


    var initApp = function (afterLoginView) {
        console.log('initializing app...');

        // init service classes
        db = new cloud.Database();
        datamodel = new cloud.DataModel();
        camera = new cloud.Camera();
        upload = new cloud.Upload();
        download = new cloud.Download();
        utils = new cloud.Utils();

        app.afterLoginView = afterLoginView;

        console.log('connected to the internet: '+db.remoteEnabled());

        // get salesforce mobile sdk oauth plugin
        oauthPlugin = cordova.require("com.salesforce.plugin.oauth");

        if (storage.getItem('isAuthenticated') == 'true') {
            console.log('already authenticated!');
            // save credentials locally
            var credentials = {
                'clientId': storage.getItem('clientId'),
                'loginUrl': storage.getItem('loginUrl'),
                'accessToken': storage.getItem('accessToken'),
                'instanceUrl': storage.getItem('instanceUrl'),
                'refreshToken': storage.getItem('refreshToken')
            };
            initForceTKClient(credentials);

            loadModals();

            datamodel.updateSObjectModels();

            app.afterLoginView();
        } else {
            console.log('not authenticated! trying authentication...');
            authenticateUser(app.afterLoginView, app.goToFailedLogin);
        }

        /****************** app level header & modal event listeners ******************/
        $('#contactListLink').on('click', function () {
            $('nav.bar-tab a.tab-item').removeClass('active');
            $(this).addClass('active');
            cloud.controller.ContactList();
        });

        $('#searchLink').on('click', function () {
            // opens the modal
            $("#searchModal").addClass("active");
        });
 
        $('#userLogoutLink').on('click', function () {
            storage.setItem('isAuthenticated', false);
            storage.setItem('clientId', null);
            oauthPlugin.logout();
        });
        /**************** end app level header & modal event listeners ****************/
    }
    app.initApp = initApp;


    /**
    * @description globally available (non-page specific) modals get initialized here such as building search
    */
    var initModals = function () {
        console.log('init modals...');

        // instantiate modal controllers for this view
        cloud.controller.modal.ContactDetail();
    }

    var loadModals = function () {
        console.log('loading modals...');
        var vl = new cloud.ViewLoader('/views/modals.html', '#modals');
        vl.loadView(initModals);
    }

    var initForceTKClient = function (credentials) {
        console.log('credentials.loginUrl: '+credentials.loginUrl);
        console.log('credentials.clientId: '+credentials.clientId);
        // Create forcetk client instance for rest API calls
        forceClient = new forcetk.Client(credentials.clientId, credentials.loginUrl);
        forceClient.setSessionToken(credentials.accessToken, "v36.0", credentials.instanceUrl);
        forceClient.setRefreshToken(credentials.refreshToken);
    }
    app.initForceTKClient = initForceTKClient;

    /**
    * @description authenticates user with Salesforce Mobile SDK's OAuth Plugin
    */
    var authenticateUser = function(afterLoginView, errorCallback) {
        // Call getAuthCredentials to get the initial session credentials
        oauthPlugin.getAuthCredentials(
            // Callback method when authentication succeeds.
            function (creds) {
                console.log('creds: '+JSON.stringify(creds));
                // save user id
                storage.setItem('userId', creds.userId);
                // save credentials locally
                storage.setItem('clientId', creds.clientId);
                storage.setItem('loginUrl', creds.loginUrl);
                storage.setItem('accessToken', creds.accessToken);
                storage.setItem('instanceUrl', creds.instanceUrl);
                storage.setItem('refreshToken', creds.refreshToken);

                initForceTKClient(creds);

                storage.setItem('isAuthenticated', 'true');

                loadModals();

                datamodel.updateSObjectModels();

                // init app start view
                afterLoginView();
            },
            errorCallback
        );
    }
    app.authenticateUser = authenticateUser;

    var goToFailedLogin = function () {
        storage.setItem('isAuthenticated', 'false');
        console.log('authentication failed!');
        // redirect to error page?
    }
    app.goToFailedLogin = goToFailedLogin;

    /* Wait until cordova is ready to initiate the use of cordova plugins and app launch */
    document.addEventListener("deviceready", function() {
        // init app and load building inspection list view when init completes
        initApp(cloud.controller.ContactList);
    }, false);

})();